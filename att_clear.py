#!/usr/bin/env python

import sys
from struct import pack, unpack
import zkem.zkem as zkem

# Put IP address of clock here
SERVER = ''

print 'This will clear the attendance log from the clock!'
answer = raw_input('Type YES to clear the log: ')

if answer != 'YES':
    print 'Stopping...'
    sys.exit()

term = zkem.zem560()
if(term.connect(SERVER)):
    if(term.clear_attendance_log()):
        print 'Attendance logs cleared'
    else:
        print 'Problem accoured, clock not cleared'

    term.disconnect()

else:
    print 'Failed to connect to %s' % SERVER
