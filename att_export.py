#!/usr/bin/env python

from sys import argv
from struct import pack, unpack
import csv
import zkem.zkem as zkem

# Put IP address of clock here
SERVER = ''

f = open('attendance.csv', 'wt')
w = csv.writer(f)

w.writerow(['userid','jnka','timestamp','state','jnkb', 'jnkc', 'workcode'])

term = zkem.zem560()
if(term.connect(SERVER)):
    if(term.get_attendance_log()):
       att = term.unpack_attendance_log()
       for i in att:
           w.writerow(i)

    term.disconnect()

else:
    print 'Failed to connect to %s' % SERVER

f.close()
